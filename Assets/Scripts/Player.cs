﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]

public class Player : MonoBehaviour
{

    public float speed = 150;
    public float linearDrag = 10;
    public bool isFacingRight = true;
    private Vector3 direction;
    private float h;
    private Rigidbody2D body;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        body.freezeRotation = true;
        Game.gameOver = false;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.transform.tag == "Finish") Game.gameOver = true;
    }

    void FixedUpdate()
    {
        body.AddForce(direction * body.mass * speed);
        if (Mathf.Abs(body.velocity.x) > speed / 100f)
        {
            body.velocity = new Vector2(Mathf.Sign(body.velocity.x) * speed / 100f, body.velocity.y);
        }
    }

    void OnBecameInvisible()
    {
        Game.gameOver = true;
        Destroy(gameObject);
    }

    // отражение по горизонтали
    void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void Update()
    {
        //Клик мыши
        
        if (Input.GetMouseButton(0))
        {
            //Получаем координаты мыши и размер дисплея
            Vector3 clickedPosition = Input.mousePosition;
            //Переводим координаты клика мыши в координаты "игрового мира"
            Vector3 cpos= Camera.main.ScreenToWorldPoint(clickedPosition);
            //Сравниваем координаты с координатми объекта
            h = cpos.x > body.position.x ? 1 : (cpos.x == body.position.x)?0 :-1;
        } else
        {
            h = Input.GetAxis("Horizontal");
        }
        
       direction = new Vector2(h, 0);
/*
        if (h > 0 && !isFacingRight)
        {
          //  Debug.Log("Right " + h);
          //  Flip();
        }
        else if (h < 0 && isFacingRight)
        {
           // Debug.Log("Left " + h);
            //Flip();
        }

      //  if (body.velocity.y == 0) body.drag = linearDrag; else body.drag = 0;
      */
    }
}
