﻿using UnityEngine;
using System.Collections;

public class Buttons3 : MonoBehaviour {

    public Sprite exit_normal, exit_small;
    public string action;

    void OnMouseDown()
    {
        GetComponent<SpriteRenderer>().sprite = exit_small;
    }

    void OnMouseUp()
    {
        GetComponent<SpriteRenderer>().sprite = exit_normal;
    }
    void OnMouseUpAsButton()
    {
        switch (action)
        {

            case "exit":
                Application.LoadLevel("exit");
                break;


        }
    }
}
