﻿
using UnityEngine;
using System.Collections;

public class FonMove: MonoBehaviour
{

    void OnCollisionEnter2D(Collision2D coll)
    {
        coll.transform.parent = transform;
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        coll.transform.parent = null;
    }

    void Update()
    {
        transform.Translate(Vector3.down * Game.speed/2 * Time.deltaTime);
        if (transform.position.y < -8.93f) // если облако уходит за пределы камеры то  "теле портируется" вверх
        {
            transform.position = new Vector3(0f, 10.5f, 0f);
        }

    }

    void OnBecameInvisible ()
    {
        Destroy(gameObject);
    }
}

