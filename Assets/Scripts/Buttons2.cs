﻿using UnityEngine;
using System.Collections;

public class Buttons2 : MonoBehaviour {

    public Sprite options_normal, options_small;
    public string action;

    void OnMouseDown()
    {
        GetComponent<SpriteRenderer>().sprite = options_small;
    }

    void OnMouseUp()
    {
        GetComponent<SpriteRenderer>().sprite = options_normal;
    }
    void OnMouseUpAsButton()
    {
        switch (action)
        {

            case "options":
                Application.LoadLevel("options");
                break;


        }
    }
}
